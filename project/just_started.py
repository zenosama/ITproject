from flask import Flask, render_template, request, flash, redirect, url_for, session, logging
# from flask_mysqldb import MySQL
import time
import datetime
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
import hashlib
import sqlite3
from functools import wraps
import random
from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer, SignatureExpired


class LoginForm(Form):
    username = StringField('')
    password = StringField('')


class RegiserForm(Form):
    m1_first = StringField('First Name of Half', [validators.Length(min=1, max=20)])
    m1_last = StringField('Last Name of Half', [validators.Length(min=1, max=20)])
    m1_email = StringField('Email of Half', [validators.Length(min=2, max=100)])

    m2_first = StringField('First Name of Other Half', [validators.Length(min=1, max=20)])
    m2_last = StringField('Last Name of Other Half', [validators.Length(min=1, max=20)])
    m2_email = StringField('Email of Other Half', [validators.Length(min=2, max=100)])

    # m1_username = StringField('Username', [validators.Length(min=2, max=30)])
    # password = PasswordField('Password', [
    #     validators.DataRequired(),
    #     validators.EqualTo('confirm', message = 'Passwords do not match')
    # ])
    # confirm = PasswordField('Confirm Password')

app = Flask(__name__)
app.config.from_pyfile('config.cfg')

mail = Mail(app)


# con = sqlite3.connect('database.db')
# cur = con.cursor()
#
# Cursor.execute("""CREATE TABLE users (
#     id AUTO_INCREMENT PRIMARY KEY,
#     first VARCHAR(20),
#     last VARCHAR(20),
#     email VARCHAR(100),
#     username VARCHAR(30) UNIQUE,
#     password VARCHAR(100),
#     register_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
#     )""")

def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))
    return wrap


def authenticate(username, password):

    con = sqlite3.connect('database.db')
    cur = con.cursor()

    # user = cur.execute("SELECT * FROM users WHERE username=''"+userrname+"'")
    cur.execute("SELECT username, password FROM users WHERE username=?", [username])

    # encrypt_pass = hashlib.sha256(str(password))
    # hashpass = encrypt_pass.hexdigest()

    hashpass = password

    user = cur.fetchone()
    cur.close()
    con.close()
    if user:
        corr_pass = user[1]
        if corr_pass == hashpass:
            # logged in
            session['logged_in'] = True
            session['username'] = username
            flash('You are now logged in', 'success')
            return render_template('layout.html')
        else:
            flash('Incorrect Password', 'danger')
            return render_template('login.html')
    else :
        flash('No such user exists', 'warning')
        return render_template('login.html')

def verifyAndRegister(username1, otp1, password1, username2, otp2, password2, token):

    con = sqlite3.connect('database.db')
    cur = con.cursor()

    cur.execute("SELECT * FROM users WHERE username=? or username=?",[username1,username2])
    check_user = cur.fetchall()
    if check_user:
    	flash('Username already exists', 'danger')
    	cur.close()
    	con.close()
    	return render_template('verify.html')
    else:
        temp_link = url_for('verify',token=token,_external=True)

        # encrypt_pass = hashlib.sha256(str(password1))
        # hashpass1 = encrypt_pass.hexdigest()

        # encrypt_pass = hashlib.sha256(str(password2))
        # hashpass2 = encrypt_pass.hexdigest()

        hashpass1 = password1
        hashpass2 = password2

        cur.execute("SELECT otp FROM users WHERE link=? and otp=?", [temp_link,otp1])
        user1 = cur.fetchone()
        if user1:
            cur.execute("SELECT otp FROM users WHERE link=? and otp=?", [temp_link,otp2])
            user2 = cur.fetchone()
            if user2:
                cur.execute("UPDATE users SET otp =?, username=? , password=? WHERE link=? and otp=?",[-1,username1,hashpass1,temp_link,otp1])
                con.commit()
                cur.execute("UPDATE users SET otp =?, username=? , password=? WHERE link=? and otp=?",[-1,username2,hashpass2,temp_link,otp2])
                con.commit()
                cur.close()
                con.close()
                flash('You are now Registered as a Couple', 'success')
                return redirect(url_for('home'))
            else:
                flash('Wrong OTP for second Half', 'warning')
                cur.close()
                con.close()
                return render_template('verify.html')
        else:
            flash('Wrong OTP for first Half', 'warning')
            cur.close()
            con.close()
            return render_template('verify.html')

        # if user:
        #     if user[0] == otp:
        #         cur.execute("UPDATE users SET otp =?, username=? , password=? WHERE link=? ",[-1,username,hashpass,temp_link])
        #         con.commit()
        #         flash('You are now successfully registered', 'success')
        #         con.close()
        #         con.close()
        #         return render_template('layout.html')
        #     else:
        #         flash('WRONG OTP', 'warning')

        cur.close()
        con.close()
        flash('You are now Registered as a Couple', 'success')
        return redirect(url_for(''))




def storeUser(m1_first, m1_last, m1_email, m2_first, m2_last, m2_email):

    con = sqlite3.connect('database.db')
    cur = con.cursor()

    cur.execute("SELECT * FROM users WHERE email=? or email=?",[m1_email, m2_email])
    check_email = cur.fetchone()
    if check_email:
    	cur.close()
    	con.close()
    	return False
    else:
   # if not cur.fetchone():
	    otp1 = random.randint(1000,1000000)
	    otp2 = random.randint(1000,1000000)

	    s = URLSafeTimedSerializer('Thisisasecret!')

	    token1 = s.dumps(m1_email, salt='email-confirm')
	    msg1 = Message('Confirm Email', sender='mausamkeda@gmail.com', recipients=[m1_email])
	    link1 = url_for('verify', token=token1, _external=True)
	    msg1.body = 'Your CoupleDal verification link is {} and otp is {}'.format(link1, otp1)
	    mail.send(msg1)

	    msg2 = Message('Confirm Email', sender='mausamkeda@gmail.com', recipients=[m2_email])
	    msg2.body = 'Your CoupleDal verification link is {} and otp is {}'.format(link1, otp2)
	    mail.send(msg2)


	    cur.execute("INSERT INTO users(first, last, email, otp, link, other_half) VALUES(?, ?, ?, ?, ?, ?)", (m1_first, m1_last, m1_email, otp1, link1, m2_email))
	    con.commit()
	    cur.execute("INSERT INTO users(first, last, email, otp, link, other_half) VALUES(?, ?, ?, ?, ?, ?)", (m2_first, m2_last, m2_email, otp2, link1, m1_email))
	    con.commit()

	    cur.close()
	    con.close()
	    return True


@app.route('/')
@app.route('/home')
def home():
    return render_template('layout.html')

@app.route("/index")
@is_logged_in
def index():
	return render_template('layout.html')


@app.route("/register", methods=["POST","GET"])
def register():
    form = RegiserForm(request.form)
    if request.method == 'POST' and form.validate():

        m1_first = form.m1_first.data
        m1_last = form.m1_last.data
        m1_email = form.m1_email.data

        m2_first = form.m2_first.data
        m2_last = form.m2_last.data
        m2_email = form.m2_email.data

        # username = form.username.data
        # password = (form.password.data)

        if storeUser(m1_first, m1_last, m1_email, m2_first, m2_last, m2_email):
            flash('Visit the link sent on your email to complete your registeration', 'success')
            return redirect(url_for('home'))
        else:
            flash('This email is already in use', 'warning')
        return render_template('register.html', form = form)
    return render_template('register.html', form = form)



@app.route('/verify/<token>', methods=['POST', 'GET'])
def verify(token):

	if request.method == 'POST':

		username1 = request.form['username1']
		password1 = request.form['password1']
		confirm1 = request.form['confirm1']
		otp1 = request.form['otp1']

		username2 = request.form['username2']
		password2 = request.form['password2']
		confirm2 = request.form['confirm2']
		otp2 = request.form['otp2']

		if confirm1 == password1 and confirm2 == password2:
			return verifyAndRegister(username1, otp1, password1, username2, otp2, password2, token)
		else:
			flash("Passwords Do Not Match", "warning")

	# flash(url_for('verify', token=token , _external=True))

	return render_template('verify.html')

@app.route('/login', methods=["POST","GET"])
def login():
    # error = "No Such User Exists"
    if request.method == 'POST':

        username = request.form['username']
        password = request.form['password']
        # error = "Invalid user or password"

        # flash('POST', 'success')
        return authenticate(username, password)


    return render_template('login.html')

@app.route('/logout', methods=["POST", "GET"])
def logout():
    session.clear()
    flash('You are now logged out', 'success')
    return redirect(url_for('home'))


if __name__ == "__main__":
    app.secret_key="1234"
    app.run(debug=True)
# cur.close()
# con.close()
